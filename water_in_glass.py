import sys


class invalidInput(Exception):
    pass


class fillWaterInGlassGeneric(object):

    def __init__(self, left_portion, right_portion, i, j, total_water, capacity):
        self.i = i
        self.j = j
        self.total_water = total_water
        self.capacity = capacity
        self.left_portion = left_portion
        self.right_portion = right_portion
        total_glasses = (self.i + 1) * (self.i + 2) // 2  # i starts from 0
        self.storage = [0] * total_glasses

    def get_water_volume_in_glass(self):
        total_rows = self.i + 1
        self.fill_water(total_rows)
        row_offset = self.i * (self.i + 1) // 2  # skip these many glasses to reach row of requested glass

        return self.storage[row_offset + self.j]


    def fill_water(self, rows):
        self.storage[0] = self.total_water #start pouring all in top glass

        for row in range(rows):
            current_row_offset = row * (row+1) // 2
            for col in range(row+1):
                if self.storage[current_row_offset + col] > self.capacity: #let the water flow downwards
                    overflow_water = self.storage[current_row_offset + col] - self.capacity
                    self.storage[current_row_offset + col] = self.capacity #hold as much as capacity

                    next_row_offset = current_row_offset + (row+1)
                    if next_row_offset < len(self.storage): #pass the remaining water to two buckets below
                        self.storage[next_row_offset + col] += overflow_water * self.left_portion
                        self.storage[next_row_offset + (col+1)] += overflow_water * self.right_portion
                    else:
                        pass #spill rest of water on floor


#specialized class that shall divide water equally among below two glaases
class fillWaterInGlassEqually(fillWaterInGlassGeneric):

    def __init__(self, argv):
        parsed_args = self.parse_and_validate_inputs(argv)
        super(fillWaterInGlassEqually, self).__init__(left_portion=0.5, right_portion=0.5, **parsed_args)

    @classmethod
    def parse_and_validate_inputs(cls, argv):
        i = int(argv[1]) #row num
        j = int(argv[2]) #col num
        k = float(argv[3]) #total water
        c = float(argv[4]) if len(argv) == 5 else 0.25 #capcaity per glass

        if i < 0 or j < 0 or j > i or k < 0 or c < 0:
            raise invalidInput("Invalid input encountered")

        return {
            'i': i,
            'j': j,
            'total_water': k,
            'capacity': c
        }


if __name__ == "__main__":
    argc = len(sys.argv)
    usage = """
    --Usage if run on command line: python water_in_glass.py i j k c
    --Usage if run using docker image: docker run water_in_glass i j k c
    i = row number : positive integer
    j = column number: positive integer not larger than i
    k = total water available: positive integer or float
    c = capacity per glass: positive integer or float (optional, default 0.25)
    """
    if argc < 4 or argc > 5:
        print (usage)
        sys.exit(1)
    try:
        fill_water_eq_obj = fillWaterInGlassEqually(sys.argv)
        water_vol = fill_water_eq_obj.get_water_volume_in_glass()
    except Exception as e:
        message = e.message if hasattr(e, 'message') else e
        print (message)
        print (usage)
        sys.exit(1)
    else:
        print('Quantity of water in a glass at row %s and column %s is %s' %
              (sys.argv[1], sys.argv[2], water_vol))
