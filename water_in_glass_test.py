import unittest
import water_in_glass as target

testData = [
    ([None, 4, 3, 10, 1], 0.3125),
    ([None, 4, 3, 10, 0.250], 0.25),
    ([None, 5, 5, 12, 0.500], 0),
    ([None, 2, 1, 2, 0.250], 0.25),
    ([None, 1, 1, 1, 0.100], 0.100),
    ([None, 3, 0, 0.5, 0.001], 0.001),
    ([None, 0, 0, 0.002, 0.002], 0.002),
    ([None, 2, 1, 0.02, 0.002], 0.002),
    ([None, 6, 5, 10, 0.25], 0.25),
    ([None, 5, 4, 10, 0.5], 0.375),
    ([None, 2, 1, 2.5, 0.75], 0.125),
    ([None, 4, 3, 5, .5], 0.15625),
    ([None, 4, 2, 2, 0], 0.0),
    ([None, 0, 0, 0, 0], 0.0),
    ([None, 10, 10, 0, 0], 0.0),
    ([None, 10, 10, 0, 1], 0.0),
    ([None, 1, 1, 1, 1], 0.0),
    ([None, 2, 2, 5, 1], 0.5),
    ([None, 2, 1, 5, 1], 1),
    ([None, 2, 1, 1, 0.25], 0.125),
    ([None, 2, 1, 1], 0.125),
    ([None, 2, 2, 1], 0.0625),
    ([None, 2, 0, 1], 0.0625),
]

class TestWaterInGlass(unittest.TestCase):

    def test_water_in_glass_negative_row(self):
        with self.assertRaises(Exception) as context:
            target.fillWaterInGlassEqually.parse_and_validate_inputs([None, -1, 4, 10, 1])


        self.assertTrue('Invalid input encountered' in str(context.exception))

    def test_water_in_glass_negative_col(self):
        with self.assertRaises(Exception) as context:
            target.fillWaterInGlassEqually.parse_and_validate_inputs([None, 5, -4, 10, 1])

        self.assertTrue('Invalid input encountered' in str(context.exception))

    def test_water_in_glass_negative_total_water(self):
        with self.assertRaises(Exception) as context:
            target.fillWaterInGlassEqually.parse_and_validate_inputs([None, 5, 4, -10, 1])

        self.assertTrue('Invalid input encountered' in str(context.exception))

    def test_water_in_glass_negative_capacity(self):
        with self.assertRaises(Exception) as context:
            target.fillWaterInGlassEqually.parse_and_validate_inputs([None, 5, 4, 10, -11])

        self.assertTrue('Invalid input encountered' in str(context.exception))

    def test_water_in_glass_col_greater_than_row(self):
        with self.assertRaises(Exception) as context:
            target.fillWaterInGlassEqually.parse_and_validate_inputs([None, 5, 6, 10, -11])

        self.assertTrue('Invalid input encountered' in str(context.exception))

    def test_water_in_glass_non_integer_row(self):
        with self.assertRaises(Exception) as context:
            target.fillWaterInGlassEqually.parse_and_validate_inputs([None, '5.0', 3, 10, 11])

        self.assertTrue("invalid literal for int() with base 10: '5.0'" in str(context.exception))

    def test_water_in_glass_non_integer_col(self):
        with self.assertRaises(Exception) as context:
            target.fillWaterInGlassEqually.parse_and_validate_inputs([None, 5, '3.0', 10, 11])

        self.assertTrue("invalid literal for int() with base 10: '3.0'" in str(context.exception))

    def test_water_in_glass_invalid_input_type(self):
        with self.assertRaises(Exception) as context:
            target.fillWaterInGlassEqually.parse_and_validate_inputs([None, 5, a, 10, 11])


        self.assertTrue("name 'a' is not defined" in str(context.exception))

def create_test_cases (test_data, expected_result):
    def water_in_glass_valid_inputs(self):
        obj = target.fillWaterInGlassEqually(test_data)
        result = obj.get_water_volume_in_glass()
        self.assertEqual(result, expected_result)
    return water_in_glass_valid_inputs


if __name__ == '__main__':

    for test_data, expected_result in testData:
        test_method = create_test_cases(test_data, expected_result)
        test_method.__name__ = 'test_water_in_glass_valid_inputs_%s' % str(test_data)
        setattr(TestWaterInGlass, test_method.__name__, test_method)

    unittest.main()

