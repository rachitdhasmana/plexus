FROM python:latest

RUN mkdir /home/waterInGlass
COPY water_in_glass.py /home/waterInGlass/water_in_glass.py

ENTRYPOINT ["python", "/home/waterInGlass/water_in_glass.py"]